package main

import (
	"fmt"
	"github.com/rjeczalik/notify"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"xantios.nl/PlexWatcher/plex"
)

type Watchers struct {
	Id   string
	Name string
	Path string
}

func handler(watcher Watchers, event notify.EventInfo) {
	// fmt.Printf("File event :: %#v in Watcher :: %#v \n", event, watcher)
	fmt.Printf("Updating library %s\n", watcher.Name)
	err := client.UpdateLibrary(watcher.Id)
	if err != nil {
		fmt.Printf("Error while updating :: %s\n", err.Error())
		return
	}
}

var watchers = make([]Watchers, 0)
var server = GetServer()
var folders = GetWatchFolders()
var client *plex.Plex

func main() {

	fmt.Printf("Connecting to %s\n", server.URL)

	client = plex.New(server.URL)
	token := GetToken(client)
	client.SetToken(token)

	// Get libs
	libraries, err := client.GetLibraries()
	if err != nil {
		fmt.Printf("Cant get libraries from Plex")
		return
	}

	// Map libraries to paths
	for _, item := range libraries {
		libName := strings.ToLower(item.Title)
		_, ok := folders[libName]
		if !ok {
			fmt.Printf("Missing mapping in config.yaml for library <%s>", libName)
			continue
		}

		for _, remoteLocation := range item.Location {
			watchers = append(watchers, Watchers{
				Id:   strconv.Itoa(remoteLocation.Id),
				Name: libName,
				Path: folders[libName],
			})
		}
	}

	for _, item := range watchers {

		// Overview
		fmt.Printf("Mapping library \"%s\" to %s (id: %s)\n", item.Name, item.Path, item.Id)

		go func(item Watchers) {

			c := make(chan notify.EventInfo)

			// Add three dots at the end of the path to make it recursive
			e := notify.Watch(item.Path+"...", c, notify.All)

			if e != nil {
				fmt.Printf("Cant watch path %s \n", item.Path)
				return
			}

			for {
				msg := <-c
				handler(item, msg)
			}

		}(item)
	}

	// Handle ^C for debug purpose
	ctrlC := make(chan os.Signal, 1)
	signal.Notify(ctrlC, os.Interrupt)
	go func() {
		for range ctrlC {
			os.Exit(0)
		}
	}()

	// Wait for ever and ever and ever
	for {
	}
}

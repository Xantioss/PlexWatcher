package watcher

import (
	"github.com/fsnotify/fsnotify"
	"log"
)

var fsnotifyWatcher *fsnotify.Watcher

func init() {
	var err error
	fsnotifyWatcher, err = fsnotify.NewWatcher()
	if err != nil {
		panic("Cant create FS watchers, maybe your OS is not supported?")
	}
}

func Run(onChange func(fsnotify.Event)) {
	for {
		select {
		case event, ok := <-fsnotifyWatcher.Events:
			if !ok {
				return
			}
			log.Println("event:", event)
			onChange(event)
		case err, ok := <-fsnotifyWatcher.Errors:
			if !ok {
				return
			}
			log.Println("error:", err)
		}
	}
}

func Add(path string) error {
	return fsnotifyWatcher.Add(path)
}

func Close() {
	fsnotifyWatcher.Close()
}

package plex

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"runtime"
	"strconv"
	"time"
)

const (
	applicationXml  = "application/xml"
	applicationJson = "application/json"
)

// headers headers to use for requests to Plex Cloud
type headers struct {
	Platform               string
	PlatformVersion        string
	Provides               string
	Product                string
	Version                string
	Device                 string
	ContainerSize          string
	ContainerStart         string
	Token                  string
	Accept                 string
	ContentType            string
	ClientIdentifier       string
	TargetClientIdentifier string
}

// PinValidationResponse Json body when polling for auth token
type PinValidationResponse struct {
	Id               int    `json:"id"`
	Code             string `json:"code"`
	Product          string `json:"product"`
	Trusted          bool   `json:"trusted"`
	Qr               string `json:"qr"`
	ClientIdentifier string `json:"clientIdentifier"`
	Location         struct {
		Code                       string `json:"code"`
		EuropeanUnionMember        bool   `json:"european_union_member"`
		ContinentCode              string `json:"continent_code"`
		Country                    string `json:"country"`
		City                       string `json:"city"`
		TimeZone                   string `json:"time_zone"`
		PostalCode                 string `json:"postal_code"`
		InPrivacyRestrictedCountry bool   `json:"in_privacy_restricted_country"`
		Subdivisions               string `json:"subdivisions"`
		Coordinates                string `json:"coordinates"`
	} `json:"location"`
	ExpiresIn       int       `json:"expiresIn"`
	CreatedAt       time.Time `json:"createdAt"`
	ExpiresAt       time.Time `json:"expiresAt"`
	AuthToken       string    `json:"authToken"`
	NewRegistration bool      `json:"newRegistration"`
}

// ErrorResponse contains a code and an error message
type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// PinResponse holds information to successfully check a pin when linking an account
type PinResponse struct {
	ID               int             `json:"id"`
	Code             string          `json:"code"`
	ClientIdentifier string          `json:"clientIdentifier"`
	CreatedAt        string          `json:"createdAt"`
	ExpiresAt        string          `json:"expiresAt"`
	ExpiresIn        json.Number     `json:"expiresIn"`
	AuthToken        string          `json:"authToken"`
	Errors           []ErrorResponse `json:"errors"`
	Trusted          bool            `json:"trusted"`
	Location         struct {
		Code         string `json:"code"`
		Country      string `json:"country"`
		City         string `json:"city"`
		Subdivisions string `json:"subdivisions"`
		Coordinates  string `json:"coordinates"`
	}
}

// Plex Default client struct
type Plex struct {
	URL              string
	Token            string
	ClientIdentifier string
	Headers          headers
	HTTPClient       http.Client
}

func defaultHeaders() headers {
	version := "0.0.1"

	return headers{
		Platform:         runtime.GOOS,
		PlatformVersion:  "0.0.1",
		Product:          "Plex Watcher",
		Version:          version,
		Device:           runtime.GOOS + " " + runtime.GOARCH,
		ClientIdentifier: "golang-client-v" + version,
		Accept:           applicationJson,
		ContentType:      applicationJson,
	}
}

func New(url string) *Plex {

	var p Plex

	p.URL = url
	p.HTTPClient = http.Client{Timeout: time.Second * 3}
	p.Headers = defaultHeaders()

	p.ClientIdentifier = p.Headers.ClientIdentifier
	p.Headers.ClientIdentifier = p.ClientIdentifier

	return &p
}

func (p *Plex) SetToken(token string) {
	p.Token = token
}

func (p *Plex) CheckPin(challenge PinResponse) (string, error) {

	url := "https://plex.tv/api/v2/pins/" + strconv.Itoa(challenge.ID)
	payload := []byte("X-Plex-Client-Identifier=" + p.Headers.ClientIdentifier)

	req, _ := http.NewRequest("GET", url, bytes.NewBuffer([]byte(payload)))
	req.Header.Set("Accept", applicationJson)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Content-Length", strconv.Itoa(len(payload)))

	x, err := p.HTTPClient.Do(req)
	out := PinValidationResponse{}

	if err != nil {
		return "", err
	}

	reply, _ := io.ReadAll(x.Body)
	json.Unmarshal(reply, &out)

	if out.AuthToken == "" {
		return "", errors.New("not authenticated yet")
	}

	return out.AuthToken, nil
}

func (p *Plex) GetPin() (PinResponse, error) {

	url := "https://plex.tv/api/v2/pins"
	payload := []byte("polling=1&strong=false&X-Plex-Product=" + p.Headers.Product + "&X-Plex-Client-Identifier=" + p.Headers.ClientIdentifier)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer([]byte(payload)))
	req.Header.Set("Accept", applicationJson)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Content-Length", strconv.Itoa(len(payload)))

	x, err := p.HTTPClient.Do(req)

	if err != nil {
		fmt.Printf("Error :: %#v\n", err)
		return PinResponse{}, err
	}

	reply, _ := io.ReadAll(x.Body)
	pin := PinResponse{}

	json.Unmarshal(reply, &pin)
	return pin, nil
}

package plex

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
)

type LibraryLocation struct {
	Id   int    `json:"id"`
	Path string `json:"path"`
}

type LibraryDirectory struct {
	AllowSync        bool              `json:"allowSync"`
	Art              string            `json:"art"`
	Composite        string            `json:"composite"`
	Filters          bool              `json:"filters"`
	Refreshing       bool              `json:"refreshing"`
	Thumb            string            `json:"thumb"`
	Key              string            `json:"key"`
	Type             string            `json:"type"`
	Title            string            `json:"title"`
	Agent            string            `json:"agent"`
	Scanner          string            `json:"scanner"`
	Language         string            `json:"language"`
	Uuid             string            `json:"uuid"`
	UpdatedAt        int               `json:"updatedAt"`
	CreatedAt        int               `json:"createdAt"`
	ScannedAt        int               `json:"scannedAt"`
	Content          bool              `json:"content"`
	Directory        bool              `json:"directory"`
	ContentChangedAt int               `json:"contentChangedAt"`
	Hidden           int               `json:"hidden"`
	Location         []LibraryLocation `json:"Location"`
}

type LibraryResponse struct {
	MediaContainer struct {
		Size      int                `json:"size"`
		AllowSync bool               `json:"allowSync"`
		Title1    string             `json:"title1"`
		Directory []LibraryDirectory `json:"Directory"`
	} `json:"MediaContainer"`
}

func (p *Plex) GetLibraries() ([]LibraryDirectory, error) {
	url := p.URL + "/library/sections/?X-Plex-Token=" + p.Token
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Set("Accept", applicationJson)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	x, err := p.HTTPClient.Do(req)
	if err != nil {
		return []LibraryDirectory{}, err
	}

	var out LibraryResponse
	body, _ := io.ReadAll(x.Body)
	json.Unmarshal(body, &out)

	return out.MediaContainer.Directory, nil
}

func (p *Plex) UpdateLibrary(id string) error {

	url := p.URL + "/library/sections/" + id + "/refresh?X-Plex-Token=" + p.Token
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Set("Accept", applicationJson)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	x, err := p.HTTPClient.Do(req)
	if err != nil {
		return err
	}

	if x.StatusCode == 200 {
		return nil
	}

	return errors.New("invalid status code " + strconv.Itoa(x.StatusCode))
}

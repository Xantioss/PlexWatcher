package main

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"strings"
)

type ServerConfig struct {
	Type string `yaml:"type"`
	URL  string `yaml:"url"`
}

type Config struct {
	Server ServerConfig `yaml:"server"`
	Watch  []string     `yaml:"watch"`
}

func getConfig() Config {
	if _, err := os.Stat("./config.yaml"); err != nil {
		panic("Config.yaml does not exist. see config.yaml.example")
	}

	cfg := Config{}
	file, _ := os.ReadFile("./config.yaml")
	err := yaml.Unmarshal(file, &cfg)
	if err != nil {
		panic(fmt.Sprintf("Cant read config.yaml, error while parsing: %s\n", err.Error()))
		return Config{}
	}

	return cfg
}

func GetServer() struct {
	Type string `yaml:"type"`
	URL  string `yaml:"url"`
} {
	cfg := getConfig()
	return cfg.Server
}

func GetWatchFolders() map[string]string {
	out := make(map[string]string)
	cfg := getConfig()

	for _, data := range cfg.Watch {
		item := strings.SplitN(data, ":", 2)
		out[item[0]] = item[1]
	}

	return out
}

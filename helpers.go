package main

import (
	"fmt"
	"os"
	"time"
	"xantios.nl/PlexWatcher/plex"
)

func GetToken(client *plex.Plex) string {

	// Use existing token
	if _, err := os.Stat("./TOKENFILE"); err == nil {
		file, _ := os.ReadFile("./TOKENFILE")
		return string(file)
	}

	// Create new token
	pin, _ := client.GetPin()

	fmt.Printf("Go to the following URL to login with your Plex account: http://plex.tv/link and use code %s\n\n", pin.Code)

	var token string

	for {
		token, _ = client.CheckPin(pin)
		time.Sleep(time.Second * 1)

		if token != "" {
			break
		}
	}

	fmt.Printf("Token :: %s\n", token)
	os.WriteFile("./TOKENFILE", []byte(token), 764)

	return token
}
